import React from 'react';
import logo from './gitlab.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://gitlab.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn GitLab CI
        </a>
        <p>
          Learning CI/CD through GitLab Actions  with AWS S3 Bucket <br/> 
          all the process has been autometed with the pipeline  to host the static website
          <br/>
          Environments available:
          <ul>
            <li>Production</li>
            <li>Staging</li>
          </ul>

        </p>
      </header>
    </div>
  );
}

export default App;
